{{-- Extensión del layout padre del sitio web --}}
@extends('layouts.app')

{{-- Aplicación de Yield --}}
@section('tituloPagina', 'Inicio - Visualizador de Automóviles')

{{-- Aplicación de Yield --}}
@section('contenidoPagina')
    <!-- Jumbotron -->
    <div class="jumbotron text-center">

    <!-- Title -->
    <h2 class="card-title h2">Visualizador de Automóviles</h2>
    <!-- Subtitle -->
    <p class="blue-text my-4 font-weight-bold">Cónoce Todo sobre Automóviles</p>
  
    <!-- Grid row -->
    <div class="row d-flex justify-content-center">
  
      <!-- Grid column -->
      <div class="col-xl-7 pb-2">
  
        <p class="card-text">
            Aplicación creada para el Examén del Primer parcial de la materia Programación de Aplicaciones.
             La utilidad es muy sencilla, solamente da click en el botón debajo para ver el catálogo de automóviles que existen.
        </p>
        <p class="card-text">
            Si deseas ir al panel administrativo, da click en el ícono de usuario.
        </p>
  
      </div>
      <!-- Grid column -->
  
    </div>
    <!-- Grid row -->
  
    <hr class="my-4">
  
    <div class="pt-2">
      <a href="/marcas" class="btn btn-cyan waves-effect">Marcas <span class="fas fa-car ml-1"></span></a>
    </div>
  
  </div>
  <!-- Jumbotron -->
  <br><br><br><br><br><br><br><br><br><br>
@endsection

