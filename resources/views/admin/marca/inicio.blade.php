
{{-- Extensión del layout padre del sitio web --}}
@extends('layouts.admin')

{{-- Aplicación de Yield --}}
@section('tituloAdmin', 'Admin Marcas - Visualizador de Automóviles')

{{-- Aplicación de Yield --}}
@section('contenidoAdmin')


        <div class="jumbotron text-center">
            <!-- Title -->
            <h2 class=" card-title h2">Visualizador de Automóviles</h2>
            <!-- Subtitle -->
            <p class="blue-text my-4 font-weight-bold">Listado de Marcas Registradas</p>
        </div>

        <table class="table table-hover">
            <thead>
              <tr>
                <th scope="col">Cv</th>
                <th scope="col">Marca</th>
                <th scope="col">País</th>
                <th scope="col">Descripciones</th>
                <th scope="col">Logo</th>
                <th scope="col">Interacciones</th>

              </tr>
            </thead>
            <tbody>
                @foreach ($marcas as $i => $marca)
                    <tr>
                        <th scope="row">{{$marca->Cv_Marca}}</th>
                        <td>{{$marca->Nombre}}</td>
                        <td>{{$marca->País}}}</td>
                        <td>
                            <strong>Creación:</strong><p>{{$marca->created_at}}</p>
                            <strong>Edición:</strong><p>{{$marca->updated_at}}</p>
                            <strong>Eliminación:</strong> 
                            @if ($marca->deleted_at)
                                <p>{{$marca->deleted_at}}</p>
                            @else
                                <p>Activo</p>
                            @endif
                        </td>
                        <td>
                            <img class="d-flex mb-3 mx-auto media-image img-fluid" src="{{asset('img/marcas/'.$marca->imagene->Foto)}}"
                            alt="Generic placeholder image">
                        </td>
                        <td>
                            <button type="button" class="btn btn-outline-primary waves-effect btn-sm"><i class="far fa-eye"></i></button>
                            <button type="button" class="btn btn-outline-primary waves-effect btn-sm"><i class="fas fa-pencil-alt"></i></button>                             
                            <button type="button" class="btn btn-outline-danger waves-effect btn-sm"><i class="fas fa-trash-alt"></i></button>
                        </td>
                    </tr>
                @endforeach

            </tbody>
          </table>

        <br><br><br><br><br><br><br><br><br><br>

@endsection