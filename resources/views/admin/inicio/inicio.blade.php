{{-- Extensión del layout padre del sitio web --}}
@extends('layouts.admin')

{{-- Aplicación de Yield --}}
@section('tituloAdmin', 'Admin - Visualizador de Automóviles')

{{-- Aplicación de Yield --}}
@section('contenidoAdmin')
    <!-- Jumbotron -->
    <div class="jumbotron text-center">

    <!-- Title -->
    <h2 class="card-title h2">Panel de Administración</h2>
    <!-- Subtitle -->
    <p class="blue-text my-4 font-weight-bold">...</p>
  
    <!-- Grid row -->
    <div class="row d-flex justify-content-center">
  
      <!-- Grid column -->
      <div class="col-xl-7 pb-2">
  
        <p class="card-text">
            Apartado administrativo, aquí podrás encontrar lo necesario para hacer de la página web algo dinámico.
        </p>
        <p class="card-text">
            Evidentemente, aún no se ha implementado ningún mecanismo de seguridad para esta sección.
        </p>
  
      </div>
      <!-- Grid column -->
  
    </div>
    <!-- Grid row -->
  
    <hr class="my-4">

  </div>
  <!-- Jumbotron -->
  <br><br><br><br><br><br><br><br><br><br>
@endsection

