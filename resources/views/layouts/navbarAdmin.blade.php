<!--Navbar -->
<nav class="mb-1 navbar navbar-expand-lg navbar-dark default-color">
    <a class="navbar-brand" href="#">Panel Admin</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
      aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
      <ul class="navbar-nav mr-auto">

        <li class="nav-item @if (Request::path() == '/admin') {{'active'}} @endif">
          <a class="nav-link" href="/admin">Inicio
            <span class="sr-only">(current)</span>
          </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/">Web
              <span class="sr-only">(current)</span>
            </a>
        </li>
        <li class="nav-item @if (Request::path() == '/admin/marcas') {{'active'}} @endif">
          <a class="nav-link" href="/admin/marcas">Marcas
          </a>
        </li>
        
      </ul>
      <ul class="navbar-nav ml-auto nav-flex-icons">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user-cog"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right dropdown-default"
            aria-labelledby="navbarDropdownMenuLink-333">
            <a class="dropdown-item" href="#">Acción</a>
            <a class="dropdown-item" href="#">Otra acción</a>
          </div>
        </li>
      </ul>
    </div>
  </nav>
  <!--/.Navbar -->